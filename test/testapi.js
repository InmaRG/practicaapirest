var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');
var server = require ('../server.js');
//var user = require ('../usuarios.json');
chai.use(chaihttp);
var should = chai.should();

describe('Test de API Usuarios',
 function() {
   it('Prueba que la API de usuarios responde correctamente.',
     function(done) {
       chai.request('http://localhost:3000')
         .get('/apitechu/v1')
         .end(
           function(err, res) {
             res.should.have.status(200);
             res.body.msg.should.be.eql("Mensaje API Tech U")
             done();
           }
         )
     }
   ),
   it('Prueba que la API devuelve una lista de usuarios correctos.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/users')
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.be.a("array");
           for (user of res.body) {
             user.should.have.property('email');
             user.should.have.property('pass');
           }
           done();
         }
       )
     }
   )
 }
);
