
var express = require('express');
var app = express();
var port = process.env.port || 3000;
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var baseMlabURL="https://api.mlab.com/api/1/databases/apitechuirg/collections/";
var mLabAPIKey="apiKey=ydHr1CAsisjtUK0BAObte44Xbh2NafEw";
var requestJSON = require('request-json');

app.listen(port);
console.log("API escuachado en el puerto:   " + port);
app.get('/apitechu/v1',
     function (req, res) {
       console.log("GET /apitechu/v1");
       res.send(
         {
           "msg" : "Mensaje API Tech U"
         }
       );
     }
);

app.get('/apitechu/v1/users',
     function (req, res) {
       console.log("GET /apitechu/v1/users");
       res.sendFile('usuarios.json', {root: __dirname});
     }
);

app.post('/apitechu/v1/users',
     function (req, res) {
       console.log("POST añadir usuarios");
       console.log(req);
       var newUser = {
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "country" : req.body.country,
       }

       console.log ("first_name es: " + req.body.first_name);
       console.log ("last_name es: " + req.body.last_name);
       console.log ("country es: " + req.body.country);

       var users = require('./usuarios.json');
       users.push(newUser);

       console.log ("Usuario creado con existo ");
       writeUserDataToFile(users);
       res.send ({"msg" : "Usuario añadido con exito " });
     }
);


app.delete('/apitechu/v1/users/:id',
     function (req, res) {
       console.log("DELETE eliminar usuarios");
       console.log ("ID: " + req.params.id);
       var users = require ('./usuarios.json');
       users.splice (req.params.id - 1, 1);
       writeUserDataToFile (users);
       res.send ({"msg" : "Usuario borrado con exito " });
     }
);

function writeUserDataToFile(data) {
  var fs = require ('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile (
    "./usuarios.json",
    jsonUserData,
    "utf8",
    function (err) {
        if (err) {
            console.log (err);
        }else {
          console.log("Usuario persistido");
        }
    }
  );
  }

  app.post('/apitechu/v1/monstruo/:p1/:p2',
     function (req, res) {
         console.log("Parametros");
         console.log (req.params);

         console.log("Query string");
         console.log (req.query);

         console.log("Header");
         console.log (req.headers);

         console.log("Body");
         console.log(req.body);

     }
);

/***********************14/03/2018**********/

app.post('/apitechu/v1/login',
     function (req, res) {
       console.log("Login Usuarios");
       var users = require ('./usuarios.json');
       console.log("req.body.email:" + req.body.email);
       console.log("req.body.pass:" + req.body.pass);
       var login=false;
       var userlogadoId;

       for (user of users) {

         console.log("Users:" + user.email);
         console.log("Users:" + user.pass);

         if ((req.body.email == user.email) && (req.body.pass==user.pass)) {
           console.log("Usuario correcto");
           user.logged=true;
           login=true;
           userlogadoId=user.id;
           writeUserDataToFile(users);
           break;
         }

       }//for

       if(login===true) {
         res.send ({
           "msg" : "Login correcto",
           "id" : userlogadoId
          });
       }else {
          res.send ({"msg" : "Login incorrecto " });
       }

     }// function
);

app.post('/apitechu/v1/logout',
     function (req, res) {
       console.log("Logout Usuarios");
       var users = require ('./usuarios.json');
       console.log("req.body.email:" + req.body.id);
       var logout = false;

       for (user of users) {

         console.log("Users:" + user.id);

         if (req.body.id == user.id) {
           console.log("Logout correcto");
           delete user.logged;
           writeUserDataToFile(users);
           logout=true;
           break;
         }

       }//for

       if (logout === true) {
         res.send ({
           "msg" : "Logout correcto",
           "id" : req.body.id
          });
       }else {
          res.send ({"msg" : "Logout incorrecto " });
       }

     }// function
);

/*************************Conexion a la BB.DD Mongo ********************/

app.get('/apitechu/v2/users',
     function (req, res) {
       console.log("GET /apitechu/v2/users");
       console.log("baseMlabURL: " + baseMlabURL);
       console.log("user?" + mLabAPIKey);

       httpClient = requestJSON.createClient(baseMlabURL);

       httpClient.get("user?"+ mLabAPIKey,
           function (err,resMLab,body) {
            var response = !err ? body : {
                "msg" : "Error obteniendo usuarios"
            }
              res.send(response);
            }//Funcion
         );//cierre del get
     }
);

app.get('/apitechu/v2/users/:id',
     function (req, res) {
       console.log("GET /apitechu/v2/users/:id");
       var id = req.params.id;
       var query = 'q={"id" : ' + id +'}';

       httpClient = requestJSON.createClient(baseMlabURL);
       console.log("Cliente creado");


       httpClient.get("user?"+ query + "&"+ mLabAPIKey,
       function(err, resMLab, body) {
         if (err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
           res.status(500);
          } else {
              if (body.length > 0) {
                  response = body[0];
              } else {
                  response = {
                      "msg" : "Usuario no encontrado."
                  };
              res.status(404);
            }//else
         }//else
         res.send(response);
} //function
         );//cierre del get
     }
);

app.post('/apitechu/v2/login',
     function (req, res) {
       console.log("Login Usuarios");

       console.log("req.body.email:" + req.body.email);
       console.log("req.body.pass:" + req.body.pass);
       var response;

       var query = 'q={"email":"'+req.body.email +'","pass" :"' + req.body.pass+ '"}';
       console.log("Query : " + query);

       httpClient = requestJSON.createClient(baseMlabURL);
       console.log("Cliente creado");
       httpClient.get("user?"+ query + "&"+ mLabAPIKey,
           function(err, resMLab, body) {
           if (err) {
               response = {
                "msg" : "Error obteniendo usuario."
            }
           res.status(500);
           res.send(response);
           } else {
              if (body.length > 0) {
                  //response = body[0];
                  var putBody = '{"$set":{"logged":true}}';
                  console.log(putBody);
                  var queryPut='q={"id":"'+body[0].id +'"}';
                  console.log("QueryPut: " + queryPut);
                  httpClient.put("user?"+query+"&"+mLabAPIKey,JSON.parse(putBody),
                       function(errPut,resMLabPut,bodyPut){
                         console.log("Llega al put");
                         console.log("errPut:" + errPut);
                            response = {
                             "msg" : "Usuario logado"
                            }
                            res.send(response);

                       }//funcion(errPut,resMLabPut,bodyPut)

                  )//httpClient.put(
              } else {
                  response = {
                      "msg" : "Usuario no encontrado."
                  };
              res.status(404);
              res.send(response);
            }//else if (body.length > 0)
         }//else if(err)
        }// function (err, resMLab, body)*/
      );//httpClient.get("user?"+ query + "&"+ mLabAPIKey,

   }//function (req, res)
);

app.post('/apitechu/v2/logout/:id',
 function(req, res) {
   console.log("POST /apitechu/v2/logout/:id");

   var query = 'q={"id": ' + req.params.id + '}';
   console.log("query es " + query);

   httpClient = requestJSON.createClient(baseMlabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Got a user with that id, logging out");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":""}}'
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)

/*************************Se obtiene las cuentas de un usuario********/

app.get('/apitechu/v2/users/:id/accounts',
     function (req, res) {
       console.log("Cuentas del usuario:" + req.params.id);


       var response;

       var query = 'q={"id_user":'+ req.params.id +'}';
       console.log("Query : " + query);
       console.log("baseMlabURL : " + baseMlabURL);


       httpClient = requestJSON.createClient(baseMlabURL);
       console.log("Consulta de Cuentas");
       httpClient.get("account?"+ query + "&"+ mLabAPIKey,
           function(err, resMLab, body) {
           if (err) {
               response = {
                "msg" : "Error obteniendo cuentas."
            }
           res.status(500);
           res.send(response);
           } else {
             if (body.length > 0) {
                  console.log("El usuario tiene cuentas");
                  response = !err ? body : {
                      "msg" : "Error obteniendo usuarios"
                  }
                  res.send(response);

              } else {
                  response = {
                      "msg" : "El usuario no tiene cuentas"
                  };
              res.status(404);
              res.send(response);
            }//else if (body.length > 0)*/
         }//else if(err)
        }// function (err, resMLab, body)*/
      );//httpClient.get("user?"+ query + "&"+ mLabAPIKey,

   }//function (req, res)
);
